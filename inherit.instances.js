var graphlib = require("graphlib")
var async = require('async')
var bluebird = require('bluebird')
var breadthFirst = require('./breadth.first')
module.exports = function (app, graph, root, relations) {
    /*  
        - Add an attribute called inherit on all Models. 
        - default value is false
        - In inherit relations, mark inherit as true
        - pass root as an option  - root = 'Account'
        - get a postorder from Account
        - make async function inheritFromParent
        - call this function for all models in mapSeries

        inheritFromParent
        - find and update all where inherit is false to true
    */

    var model = app.models[root]

    model.inheritInstances = inheritAllInstances

    model.remoteMethod('inheritInstances', {
        accepts: [{}],
        returns: {
            arg: 'response',
            type: 'object'
        },
        http: {
            path: '/inherit',
            verb: 'post'
        }
    })

    function inheritAllInstances(remote_callback) {
        //collect all models in this variable
        var models_in_belongsTo = {}
        relations.map(function (relation) {
            models_in_belongsTo[relation.from] = true
            models_in_belongsTo[relation.to] = true
        })

        //add inherited as a default false property on all models
        Object.keys(models_in_belongsTo).map(function (modelName) {
            app.models[modelName].defineProperty('inherited', {
                type: 'boolean',
                default: false
            })
        })

        //var top_to_bottom = graph.children(root)//graphlib.alg.preorder(graph, root)
        var top_to_bottom = breadthFirst(graph, root)
        top_to_bottom.splice(top_to_bottom.indexOf(root), 1)

        console.log('Models to inherit from top to bottom ', top_to_bottom)
        //top_to_bottom = ["Share"]

        //add a post method in all these models for inhering


        async.mapSeries(top_to_bottom, function (modelName, callback) {
            console.log('inheriting instances of ', modelName)
            inheritFromParent(modelName)
                .then(function () {
                    //console.log('2. completed inheritance for ', modelName)
                    callback(null, modelName)
                }, function (err) {
                    if (err) {
                        console.log('Error while inheriting instances for of model ', modelName)
                    }
                    callback(err)
                })
        }, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                console.log('Completed inheritance of all model instances')
            }

            remote_callback(err, result)
        })
    }





    function inheritFromParent(modelName) {
        var D = bluebird.defer()
        var model = app.models[modelName]
        var filter = { where: { inherited: { neq: true } } }//{}//

        model.find(filter)
            .then(function (instances) {
                console.log('Updating ', instances.length, ' instances of ', modelName)
                var index = 0, count = instances.length
                async.mapSeries(instances, function (instance, callback) {
                    console.log('Updating ', modelName, index++, '/', count)
                    inheritInstance(instance.id, modelName)
                        .then(function () {
                            callback(null)
                        }).catch(function (err) {
                            console.log('Error while updating instance ', instance.id, ' of ', modelName)
                            callback(err)
                        })
                }, function (err, results) {
                    if (err) {
                        D.reject(err)
                        console.log('Error while updating instances of modelName ', modelName)
                    } else {
                        //console.log('1. completed inheritance for ', modelName)
                        D.resolve(instances.length)
                    }

                })
            })

        return D.promise

    }

    function inheritInstance(id, modelName) {
        return app.models[modelName].findById(id)
            .then(function (model_instance) {
                return model_instance.updateAttribute('inherited', true)
            })
    }
}