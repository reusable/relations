var graphlib = require("graphlib")
var Graph = graphlib.Graph
var _ = require('lodash')
const util = require('util')
var inheritInstances = require('./inherit.instances')
module.exports = function (app, root, inherit, relations) {
    /*
        1. Create inheritance graph for belongsTo relationship
        2. Mark all root nodes
    */

    var belongsTo_graph = new Graph()
    var roots = {}

    relations.map(function (relation) {
        if (relation.type == 'belongsTo') {
            belongsTo_graph.setNode(relation.from)
            belongsTo_graph.setNode(relation.to)
            belongsTo_graph.setEdge(relation.to, relation.from, relation.as)
            console.log(relation.to + '-->' + relation.from)

            if (inherit.indexOf(relation.as) >= 0) {
                //this relation is to be inherited. Mark root node
                //console.log('ROOT')

                if (!roots[relation.from]) {
                    roots[relation.from] = []
                }
                roots[relation.from].push({
                    as: relation.as,
                    to: relation.to
                })

                /*
                var root = {
                    Branch :[{
                        as:'account',
                        to:'Account'
                    }],
                    RTS:[{
                        as :'poc',
                        to:'POC'
                    },{
                        as :'campaign',
                        to:'Campaign'
                    }]
                }
                */
            }
        }
    })


    console.log('Roots ', roots)
    var from_via_pairs = {}
    Object.keys(roots).map(function (rootNode) {
        roots[rootNode].map(function (target) {
            var from = Descendants(rootNode), toNode = target.to, as = target.as

            // console.log('to ', toNode, ' as ', as, ' - All children of ', rootNode)
            // console.log('From ', from)

            from.map(function (fromNode) {
                /*
                -- Get parent node that is descending from the to node
                -- Get relationshipId to the rootNode from parentNode
                -- Assign relationshipId on currentNode
                */
                var parents = belongsTo_graph.predecessors(fromNode)
                var descendants_of_to = Descendants(toNode)
                var descending_parents = _.intersection(parents, descendants_of_to)
                if (descending_parents.length >= 1) {
                    var viaNode = descending_parents[0]
                    var from_via = fromNode + '.' + viaNode
                    if (!from_via_pairs[from_via]) {
                        from_via_pairs[from_via] = {
                            from: fromNode,
                            via: viaNode,
                            to_as: []
                        }
                    }
                    from_via_pairs[from_via].to_as.push({
                        to: toNode,
                        as: as
                    })
                }
            })
        })

    })

    console.log('from via pairs ', util.inspect(from_via_pairs, false, null))

    function inheritVia(fromNode, viaNode, to_as_array) {
        //Add this relationship
        to_as_array.map(function (to_as) {
            relations.push({
                from: fromNode,
                to: to_as.to,
                as: to_as.as,
                type: 'belongsTo'
            })
        })


        /*  
            - When any node is updated            
        */
        var fromModel = app.models[fromNode]
        var parentModel = app.models[viaNode]
        var relationToParent = belongsTo_graph.edge(viaNode, fromNode)
        //console.log('relation to parent ', relationToParent)

        fromModel.observe('before save', function (ctx, next) {
            console.log('inheriting relations')
            var instance = ctx.instance || ctx.data || ctx.currentInstance
            var current_instance = ctx.currentInstance || ctx.instance || ctx.data

            //console.log('instance ', ctx.instance)
            //console.log('data ', ctx.data)
            //console.log('currentInstance ', ctx.currentInstance)
            var parentRelationId = relationToParent + 'Id'
            if (current_instance && current_instance[parentRelationId]) {
                var parent_id = current_instance[parentRelationId]
                console.log('connecting ', fromNode, current_instance.id, ' to ', toNode, ' via ', parentNode, parent_id, ' by adding ', toRelationId)
                if (parent_id) {

                    parentModel.findById(parent_id)
                        .then(function (parentInstance) {
                            if (parentInstance) {
                                to_as_array.map(function (to_as) {
                                    console.log('connecting ', fromNode, ' via ', viaNode, ' to ', to_as.to, ' by adding ', to_as.as + 'Id')

                                    var toRelationId = to_as.as + 'Id'
                                    if (parentInstance[toRelationId]) {
                                        instance[toRelationId] = parentInstance[toRelationId]
                                    }
                                })
                            }

                            instance.inherited = true
                            next()
                        }).catch(function (err) {
                            console.log('Error while connecting ', fromNode, ' to ', toNode, ' via ', parentNode, ' err-', err)
                            console.log('relationToParent is ', relationToParent, ' parent_id is ', parent_id)
                            console.log('parentInstance ', parentInstance)
                        })
                } else {
                    console.log('Unable to get parent_id for ', fromNode, instance.id, ' to ', toNode, ' via ', parentNode)
                    next()
                }
            } else {
                console.log('current instance ', current_instance)
                console.log('instance ', instance)
                next()
            }


        })

    }
    //require('./delete.extra.data')(app)
    //inherit all current instances so that they are up to date

    inheritInstances(app, belongsTo_graph, root, relations)


    function Descendants(node) {
        var desc = graphlib.alg.postorder(belongsTo_graph, node)
        desc.splice(desc.indexOf(node), 1)
        // console.log('Descendants of ', node, ' are ', desc)
        return desc
    }
}