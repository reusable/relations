var graphlib = require('graphlib')
var readlineSync = require('readline-sync');

module.exports = function (graph, root) {
    var tree = graph//graphlib.alg.prim(graph, weightFn)
    var breadth_first = [], i = 0, current = root, traversed = {}, pushed = {}
    do {
        if (!traversed[current]) {
            var successors = tree.successors(current)
            console.log('successors of ', current, ' are ', successors)
            successors.map(function (s) {
                if (!pushed[s]) {
                    breadth_first.push(s)
                    pushed[s] = true
                }

            })
        } else {
            console.log('skipping ', current)
        }
        traversed[current] = true


        console.log('breadth first ', breadth_first)

        if (i < breadth_first.length) {
            current = breadth_first[i]
        }
        console.log('moving current to ', current)

        i++
    }
    while (i <= breadth_first.length);

    var top_sorted = graphlib.alg.topsort(graph, root)

    // console.log('breadth first ', breadth_first)
    var sorted_collection = breadth_first.sort(function (a, b) {
        return top_sorted.indexOf(a) - top_sorted.indexOf(b)
    })


    // console.log('top_sorted ', top_sorted)
    // console.log('sorted_collection ', sorted_collection)

    // var c = readlineSync.keyInYNStrict('Continue?')
    // if (!c) {
    //     process.exit(0)
    // }
    return sorted_collection

    function weightFn() {
        return 1
    }
}