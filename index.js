var inheritRelations = require('./inherit.relations')
var createRelation = require('./create.relation')
module.exports = function (app, options) {
    //console.log('creating relations ', options)
    //options is the list of relations
    var relations = options.relations

    inheritRelations(app, options.root, options.inherit, options.relations)

    relations.forEach(function (relation) {

        //console.log('relation', relation)
        var fromModel = app.models[relation.from]
        var toModel = app.models[relation.to]
        var relationType = relation.type

        createRelation(fromModel, toModel, relationType, relation.as, relation.foreignKey, relation.required)

    })


}
