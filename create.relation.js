module.exports = function (fromModel, toModel, relationType, as, foreignKey, required) {
    //create the relation object parameter
    var relation_json = {
        as: as
    }

    if (foreignKey) {
        relation_json.foreignKey = foreignKey
    }

    //console.log('relation_json', relation_json)
    //create the relation
    try {
        fromModel[relationType](toModel, relation_json)
    } catch (err) {
        console.log('Error while creating relation ', fromModel, relationType, toModel, as, foreignKey, required)
        console.log(err)
    }


    //check if the relation is required
    if (required) {
        //attribute is either from the foreignKey or from relation name
        var attribute = foreignKey || (as + 'Id')
        fromModel.validatesPresenceOf(attribute)
    }
}